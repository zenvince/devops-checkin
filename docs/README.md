# How to use

This project provides a Makefile to help.

those rules requires make and Go installed
* `make dep` installs dependencies
* `make build` builds the binary
* `make run` runs the code in development
* `make test` runs tests

Those rules requires only make and docker to be installed
* `make docker_image` builds the docker image
* `make docker_testrun` runs the docker container


## Configuration

environment variables and default values

* `INTERFACE` = "0.0.0.0"
* `PORT` = 8000
* `TARGET_HOST` = "localhost"
* `TARGET_PATH` = "/"
* `TARGET_PORT` = 8000
* `TARGET_PROTO` = "http"

## Examples

```
docker run -e PORT=7999 -e TARGET_PORT=7999 devops/pinger
PORT=7999 TARGET_PORT=7999 path_to_the_binary/pinger
```


## Notes to go further

### Pipeline

```mermaid
graph TD;
    docker_image-->smoke_test;
    smoke_test-->go_binary;
```

An extra step, when a merge request makes it to the master branch, automatically tags the code and increment the version

### Containerisation 🐳

base image for production is gcr.io/distroless/static-debian10:nonroot
It only has :
* ca-certificates
* A /etc/passwd entry for a root user
* A /tmp directory
* tzdata

Documentation can be found [here](https://github.com/GoogleContainerTools/distroless/tree/master/base)

In case one wants to switch to alpine, do not forget something like this in the Dockerfile
```
RUN [ ! -e /etc/nsswitch.conf ] && echo 'hosts: files dns' > /etc/nsswitch.conf