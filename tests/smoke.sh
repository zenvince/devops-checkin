#!/bin/sh

#simply run the container for 20s and return the status code
#preserver-status is not available in busybox images
timeout 20s docker run --rm -p 8000:8000 --name pinger devops/pinger:latest || [ $? -eq 124 ] && exit 0
